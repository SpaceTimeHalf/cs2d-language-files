##### README #####

This git repo contains the CS2D language files.
CS2D is a multiplayer top-down action shooter.
Copyright © 2002-2019 Unreal Software
The original Counter-Strike is a game of the Valve Corporation. Unreal Software is not affiliated with the Valve Corporation in any way.

www.CS2D.com
www.UnrealSoftware.de


##### FILE NAMES #####

Language files should be named after this simple pattern:
'ISO-languageNameInEnglish (LanguageNameInActualLanguage).txt'

ISO should match Steam's Wep API country code (https://partner.steamgames.com/doc/store/localization) and always be ALL UPPERCASE.
It should either be 3 chars: XX e.g. 'EN-' or 6 chars: 'XX-YY-' e.g. 'PT-BR-'
If the language is not provided by Steam it should be a ISO 3166 language code (2 chars) plus hyphen (-) as separator.
If the language is artifical and has no official ISO code, you should use 'XX-' (two big X with a hyphen) as ISO, e.g.: 'XX-Klingon.txt'

e.g.: 'FR-French (Francais).txt', 'DE-German (Deutsch).txt', 'IT-Italian (Italiano).txt'

You can omit the '(LANGUAGE-NAME-IN-ACTUAL-LANGUAGE)'-part for some special cases. E.g. English itself or for artificial languages. You may also replace it with a VERY SHORT description. E.g. if there are multiple versions of the same language.

!!! ATTENTION: ONLY USE THE FOLLOWING CHARACTERS IN THE FILE NAME PLEASE !!!
- Letters from the English alphabet A-Z, a-z (https://en.wikipedia.org/wiki/English_alphabet)
- Digits 0-9
- ( and )
- . (for the .txt extension only!)
- Hyphen (-) ONLY as separator for the ISO code! Do NOT use hyphens anywhere else in the file name!


##### FILE STRUCTURE #####

The syntax is as simple as:
KEY=VALUE

With KEY being a numeric value with 4 digits (starting at 0001) and leading zeros and VALUE being the actual translation.
e.g.:
0001=Hello World!

### Comments
Comments are allowed. Lines with comments should start with //
Note: It is NOT possible to add a comment behind a KEY=VALUE line!

### Line separators
Some texts contain line separators: |
These must be applied carefully/manually so the text fits into boxes properly.
The |-separator is also used for some combobox values.

### Variables
Some texts contain variables which are replaced at runtime.
Variables start with $, followed by a number: $1, $2, ... etc.

### Colors
It is possible to define colors by adding a color code to the beginning of a line. A color code follows this pattern: ©RRRGGGBBBText
With RRR, GGG and BBB being values from 000 to 255 (always 3 digits, leading zeros). E.g.: for red: ©255000000I'm red.
Note: There is NO whitespace between color codes and text!
Attention: Only some strings can be colored!

### Custom Font (Optional)
You can optionally tell CS2D to load a custom font from gfx/fonts/ when your translation is used.
Do so by putting this at the top of the file:
font=FONTNAME.EXTENSION
e.g.:
font=chinese.ttc

If you do NOT specify a custom font, CS2D will always use the standard font (liberationsans.ttf).
This means a statement like "font=liberationsans.ttf" is ALWAYS POINTLESS and NEVER REQUIRED!
Just use no font statement to use the standard font.

### Additional Advise
It's highly recommended to always use "English.txt" as template when creating new translations.